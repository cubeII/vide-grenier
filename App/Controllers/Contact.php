<?php

namespace App\Controllers;
 use App\Utility\Mail;

class Contact extends \Core\Controller{

    public function contactFormSubmission() {
        

        if(isset($_POST) && !empty($_POST)) {
            

            // Récupérer les données du formulaire
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $emaildestinataire = $_POST['emaildestinataire'];

            //Envoyer l'email
            Mail::sendContactEmail($emaildestinataire, $name, $email, $message);
            header("Location: /");
            exit;
        }
        

    }
}