<?php

namespace App\Controllers;

use App\Models\Articles;
use App\Utility\Upload;
use \Core\View;

/**
 * Product controller
 */
class Product extends \Core\Controller
{

    /**
     * Affiche la page d'ajout
     * @return void
     */
    public function indexAction()
    {

        $errors = [];
        if(isset($_POST['submit'])) {
        $f = $_POST;
        
        // Validation
        if(empty($f['name'])){
            $errors[] = 'Le champ nom est obligatoire';
        }
        if(empty($f['description'])){
            $errors[] = 'Le champ description est obligatoire';
        }
        if(empty($_FILES['picture']['name'])){
            $errors[] = 'Le champ image est obligatoire';
        }

        if(empty($errors)){
            try {
                $f['user_id'] = $_SESSION['user']['id'];
                $id = Articles::save($f);

                $pictureName = Upload::uploadFile($_FILES['picture'], $id);

                Articles::attachPicture($id, $pictureName);

                header('Location: /product/' . $id);
                return;
            } catch (\Exception $e){
                    throw new \Exception($e);
            }
            
        }
    }

    View::renderTemplate('Product/Add.html', ['errors' => $errors]);
    }

    /**
     * Affiche la page d'un produit
     * @return void
     */
    public function showAction()
    {
        $id = $this->route_params['id'];

        try {
            Articles::addOneView($id);
            $suggestions = Articles::getSuggest();
            $article = Articles::getOne($id);
        } catch(\Exception $e){
            throw new \Exception ($e->getMessage());
        }

        View::renderTemplate('Product/Show.html', [
            'article' => $article[0],
            'suggestions' => $suggestions
        ]);
    }
}
