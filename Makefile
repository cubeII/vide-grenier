build:
	sh ./scripts/build.sh

test: build
	echo "No test"

build-docker-local: build
	docker build --rm --network=host -t grenier-en-ligne:local .

run-docker-local:
	 docker run -it --rm -p 80:80 --name grenier-en-ligne grenier-en-ligne:local

run-local:
	docker run --rm -it -p 8000:80 --name grenier-en-ligne -v ${PWD}:/var/www -v ${PWD}/apache-conf:/etc/apache2/sites-available php:7.4-apache sh /var/www/scripts/start.sh