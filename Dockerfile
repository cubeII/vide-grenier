FROM php:7.4-apache

# Setup Debian
RUN apt-get update &&\
  apt-get install --no-install-recommends --assume-yes --quiet ca-certificates curl git &&\
  rm -rf /var/lib/apt/lists/*
RUN curl -Lsf 'https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz' | tar -C '/usr/local' -xvzf -
ENV PATH /usr/local/go/bin:$PATH
RUN go get github.com/mailhog/mhsendmail
RUN cp /root/go/bin/mhsendmail /usr/bin/mhsendmail
RUN docker-php-ext-install pdo pdo_mysql


# Setup Website
RUN mkdir /var/www/vide-grenier
WORKDIR /var/www/vide-grenier
ADD . /var/www/vide-grenier/

# Setup Apache
RUN mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/backup-000-default.conf
COPY vide-grenier.conf /etc/apache2/sites-available/

RUN a2enmod rewrite
RUN a2ensite vide-grenier.conf
RUN service apache2 start

CMD ["apache2-foreground"]
