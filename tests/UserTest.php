<?php

namespace Tests\App\Controllers;

use PHPUnit\Framework\TestCase;
use App\Controllers\User; // Import de la classe User

class UserTest extends TestCase
{
    public function testLoginAction()
    {
        $this->assertTrue(method_exists(User::class, 'loginAction')); // Utilisation de la classe User
    }
}