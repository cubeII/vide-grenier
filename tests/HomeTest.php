<?php

class HomeTest extends \PHPUnit\Framework\TestCase {
    
    public function testWelcomeMessage()
    {
        $message = "This is a test";
        $this->assertEquals("This is a test", $message);
    }
}
