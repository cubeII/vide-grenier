<?php

use App\Utility\Hash;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testGenerate()
    {
        $string = 'password123';
        $salt = 'abcd1234';

        $hashedString = Hash::generate($string, $salt);

        $expectedHash = hash('sha256', $string . $salt);

        $this->assertEquals($expectedHash, $hashedString);
    }
}