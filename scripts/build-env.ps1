
# fichiers docke-compose
$dev_file = ".\build-environnement\dev-docker-compose.yml"
$pre_prod_file = ".\build-environnement\pre-prod-docker-compose.yml"
$prod_file = ".\build-environnement\prod-docker-compose.yml"

# flags
$dev_flag = "--dev"
$pre_prod_flag  = "--pre-prod"
$prod_flag  = "--prod"

if ($args[0] -eq $dev_flag) {
    $env:COMPOSE_PROJECT_NAME = "development-environment"
    docker-compose -f $dev_file up -d
} elseif ($args[0] -eq $pre_prod_flag){
    $env:COMPOSE_PROJECT_NAME = "pre-production-environment"
    git checkout stage
    docker-compose -f $pre_prod_file up -d
} elseif ($args[0] -eq $prod_flag){
    $env:COMPOSE_PROJECT_NAME = "production-environment"
    git checkout main
    docker-compose -f $prod_file up -d
}elseif ($args[0] -eq "--help"){
    Write-Host "If you experience any difficulties, please make sure to run this script from the root of the 'vide-grenier' project.`n"
    Write-Host "Available commands:"
    Write-Host "$dev_flag : build the development environment from the current branch.`n$pre_prod_flag : build the pre-production environment from the 'stage' branch.`n$prod_flag : build the production environment from the 'main' branch."
} else{
    Write-Host "Error: Invalid command or option. For help, type --help"
}
