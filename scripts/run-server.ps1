# Paramètres d'entrée

# Vérification des paramètres d'entrée
<# if (($port_flag -ge 0 -and $port_flag -lt $args.Length - 1) -and ($name_flag -ge 0 -and $name_flag -lt $args.Length - 1)) {
    $port = $args[$port_flag + 1]
    $name = $args[$name_flag + 1]
    
    # Commande Docker
    $docker_command = "docker run -tid -p" + $port + ":80 --network development-environment_default --link db-dev:db --name $name apache-dev:latest"
    Invoke-Expression $docker_command
    
    # Affichage du résultat
    Write-Host "Port : $port"
    Write-Host "Nom du conteneur : $name"
} else {
    Write-Host "Erreur : paramètres d'entrée invalides. Pour obtenir de l'aide, tapez --help"
} #>

$port_flag = $args.IndexOf("-p")
$name_flag = $args.IndexOf("-n")

# flags
$dev_flag = "--dev"
$pre_prod_flag  = "--pre-prod"
$prod_flag  = "--prod"

if (($port_flag -ge 0 -and $port_flag -lt $args.Length - 1) -and ($name_flag -ge 0 -and $name_flag -lt $args.Length - 1)){

    $port = $args[$port_flag + 1]
    $name = $args[$name_flag + 1]
    
    if ($args[0] -eq $dev_flag) {
        $docker_command = "docker run -tid -p" + $port + ":80 --network development-environment_default --link db-dev:db --name $name apache-dev:latest"
        Invoke-Expression $docker_command
    } elseif ($args[0] -eq $pre_prod_flag){
        $docker_command = "docker run -tid -p" + $port + ":80 --network pre-production-environment_default --link db-pre-prod:db --name $name apache-pre-prod:latest"
        Invoke-Expression $docker_command
    } elseif ($args[0] -eq $prod_flag){
        $docker_command = "docker run -tid -p" + $port + ":80 --network production-environment_default --link db-production:db --name $name apache-production:latest"
        Invoke-Expression $docker_command
    } else{
        Write-Host "Error: Invalid command or option. For help, type --help"
    }

} elseif ($args[0] -eq "--help"){
    Write-Host "If you experience any difficulties, please make sure to run this script from the root of the 'vide-grenier' project.`n"
    Write-Host "Available commands:"
    Write-Host "$dev_flag : run a container from the Apache server image of the development environment.`n$pre_prod_flag : run a container from the Apache server image of the pre-production environment.`n$prod_flag : run a container from the Apache server image of the production environment."
} else {
        Write-Host "Error: Invalid command or option. For help, type --help"
}
